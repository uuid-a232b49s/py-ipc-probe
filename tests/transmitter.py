import ipc_probe.probe_query as pq


def main():
    pq.query('ipc_probe_test_queue', pq.TYPE_LIVENESS)


if __name__ == '__main__':
    exit(main())
