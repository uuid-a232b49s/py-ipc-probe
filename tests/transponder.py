import ipc_probe.probe_runner as pr


def main():
    pr.loop('ipc_probe_test_queue')


if __name__ == '__main__':
    exit(main())
