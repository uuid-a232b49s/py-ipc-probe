from distutils.core import setup, Extension

setup(
    name = 'ipc_probe',
    version = '1.0',
    description = 'ipc_probe',
    packages=['ipc_probe'],
    ext_modules = [
        Extension(
            'ipc_probe_lib',
            define_macros = [
                ('IPC_MODULE_NAME', 'ipc_probe_lib'),
                ('IPC_MODULE_DESC', 'ipc_probe_lib description'),
                ('IPC_DEBUG_MODE', '0')
            ],
            libraries = ['rt'],
            sources = ['ipc_probe_lib/module.c']
        )
    ]
)
