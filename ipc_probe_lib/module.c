#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include <time.h>

#ifndef IPC_MODULE_NAME
    #error IPC_MODULE_NAME not defined
#endif
#ifndef IPC_MODULE_DESC
    #define IPC_MODULE_DESC not defined
#endif
#define IPC_CONCAT0(a, b) a##b
#define IPC_CONCAT(a, b) IPC_CONCAT0(a, b)
#define IPC_STRINGIFY0(a) #a
#define IPC_STRINGIFY(a) IPC_STRINGIFY0(a)

#if defined(IPC_DEBUG_MODE) && IPC_DEBUG_MODE
    #include <stdio.h>
    #define IPC_DBG(...) fprintf(stderr, __VA_ARGS__)
#else
    #define IPC_DBG(...)
#endif

static void calcTimeout(long long timeout, struct timespec * out) {
    {
        struct timespec t0;
        timespec_get(&t0, TIME_UTC); // may fail?
        timeout += t0.tv_sec * 1000 + t0.tv_nsec / 1.0e6;
    }
    out->tv_sec = timeout / 1000;
    out->tv_nsec = (timeout % 1000) * 1000000;
}

#include "ipc-message-queue.inl"

static PyModuleDef g_module = {
    PyModuleDef_HEAD_INIT,
    .m_name = IPC_STRINGIFY(IPC_MODULE_NAME),
    .m_doc = IPC_STRINGIFY(IPC_MODULE_DESC),
    .m_size = -1,
};

PyMODINIT_FUNC IPC_CONCAT(PyInit_, IPC_MODULE_NAME) (void) {
    IPC_DBG(IPC_STRINGIFY(IPC_CONCAT(PyInit_, IPC_MODULE_NAME))"\n");
    if (PyType_Ready(&IPCMessageQueue) != 0) { return NULL; }

    PyObject * m = PyModule_Create(&g_module);
    if (m == NULL) { return NULL; }

    Py_INCREF(&IPCMessageQueue);
    if (PyModule_AddObject(m, "IPCMessageQueue", (PyObject *) &IPCMessageQueue) != 0) {
        Py_DECREF(&IPCMessageQueue);
        return NULL;
    }

    return m;
}
