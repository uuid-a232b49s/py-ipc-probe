#include <time.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>

typedef struct {
    char * queueName;
    mqd_t queue;
    unsigned char unlinkOnDtor;
} IPCMessageQueueData_;

typedef struct {
    PyObject_HEAD
    IPCMessageQueueData_ data;
} IPCMessageQueueData;

static PyObject * IPCMessageQueue_new(PyTypeObject * type, PyObject * args, PyObject * kwds) {
    IPC_DBG("IPCMessageQueue_new\n");
    IPCMessageQueueData * self = (IPCMessageQueueData *) type->tp_alloc(type, 0);
    if (self == NULL) { return NULL; }
    self->data.queue = -1;
    self->data.unlinkOnDtor = 1;
    self->data.queueName = NULL;
    return (PyObject *) self;
}

static int IPCMessageQueue_init(IPCMessageQueueData * self, PyObject * args, PyObject * kwds) {
    IPC_DBG("IPCMessageQueue_init\n");
    static char * kwlist[] = {
        // required
        "name",
        "capacity",
        "msg_size",
        "keep_on_del",
        //"open_perm",
        "read",
        "write",
        "create",
        "exclusive",
        "non_blocking",
        NULL
    };
    const char * name = NULL;
    long int capacity = 0;
    long int msg_size = 0;
    int keep_on_del = 0;
    int open_perm = 0660;
    int read0 = 0, write0 = 0, create0 = 0, exclusive0 = 0, non_blocking0 = 0;

    IPCMessageQueueData_ data = { .queue = -1 };

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "$sllpppppp", kwlist,
        &name,
        &capacity,
        &msg_size,
        &keep_on_del,
        //&open_perm,
        &read0,
        &write0,
        &create0,
        &exclusive0,
        &non_blocking0
    )) { return -1; }

    int errno0;
    {
        int oflag = 0;
        if (read0) {
            oflag |= (write0 ? O_RDWR : O_RDONLY);
        } else if (write0) { oflag |= O_WRONLY; }
        if (create0) { oflag |= O_CREAT; }
        if (exclusive0) { oflag |= O_EXCL; }
        if (non_blocking0) { oflag |= O_NONBLOCK; }

        struct mq_attr attr = {
            .mq_maxmsg = capacity,
            .mq_msgsize = msg_size
        };

        Py_BEGIN_ALLOW_THREADS
        data.queue = mq_open(name, oflag, open_perm, &attr);
        errno0 = errno;
        Py_END_ALLOW_THREADS
    }
    if (data.queue == -1) {
        switch (errno0) {
            /*case EACCES:
                PyErr_SetString(PyExc_ValueError, "The queue exists, but the caller does not have permission to open it in the specified mode");
                break;*/
            case EEXIST:
                PyErr_SetString(PyExc_ValueError, "Both <create> and <exclusive> were specified, but a queue with this name already exists");
                break;
            case EMFILE:
                PyErr_SetString(PyExc_ValueError, "The per-process limit on the number of open file and message queue descriptors has been reached");
                break;
            case ENFILE:
                PyErr_SetString(PyExc_ValueError, "The system-wide limit on the total number of open files and message queues has been reached");
                break;
            case ENAMETOOLONG:
                PyErr_SetString(PyExc_ValueError, "Name was too long");
                break;
            /*case ENOENT:
                PyErr_SetString(PyExc_ValueError, "The <create> flag was not specified, and no queue with this name exists");
                break;*/
            case ENOMEM:
                PyErr_SetString(PyExc_ValueError, "Insufficient memory");
                break;
            case ENOSPC:
                PyErr_SetString(PyExc_ValueError, "Insufficient space for the creation of a new message queue");
                break;
            default:
                PyErr_SetString(PyExc_ValueError, "Failed to open queue");
                break;
        }
        goto lbl_err;
    }

    {
        data.unlinkOnDtor = !keep_on_del;
        data.queueName = malloc(strlen(name) + 1);
        if (data.queueName == NULL) {
            PyErr_SetString(PyExc_ValueError, "Failed to store name");
            goto lbl_err;
        }
        strcpy(data.queueName, name);
    }

    self->data = data;

    return 0;

    lbl_err: {
        if (data.queue != -1) {
            Py_BEGIN_ALLOW_THREADS
            if (data.queueName != NULL) { mq_unlink(data.queueName); }
            mq_close(data.queue);
            Py_END_ALLOW_THREADS
        }
        if (data.queueName != NULL) { free(data.queueName); }
        return -1;
    }
}

static int IPCMessageQueue_clear(IPCMessageQueueData * self) {
    IPCMessageQueueData_ data = self->data;
    self->data.queue = -1;
    self->data.queueName = NULL;
    Py_BEGIN_ALLOW_THREADS
    const char * qn = data.queueName ? data.queueName : "<unknown>";
    if (data.unlinkOnDtor && data.queueName) {
        if (mq_unlink(data.queueName) != 0) { IPC_DBG("failed to unlink queue '%s'\n", qn); }
    }
    if (data.queue != -1) {
        if (mq_close(data.queue) != 0) { IPC_DBG("failed to close queue '%s'\n", qn); }
    }
    free(data.queueName);
    Py_END_ALLOW_THREADS
    return 0;
}

static void IPCMessageQueue_dealloc(IPCMessageQueueData * self) {
    IPC_DBG("IPCMessageQueue_dealloc\n");
    IPCMessageQueue_clear(self);
    Py_TYPE(self)->tp_free((PyObject *) self);
}

static PyObject * IPCMessageQueue_send(IPCMessageQueueData * self, PyObject * args) {
    IPC_DBG("IPCMessageQueue_send 0\n");
    Py_buffer buffer;
    unsigned int prio = 0;
    long long timeout = 0;
    if (!PyArg_ParseTuple(args, "y*IL", &buffer, &prio, &timeout)) { return NULL; }
    if (!PyBuffer_IsContiguous(&buffer, 'C')) {
        PyErr_SetString(PyExc_ValueError, "Illegal bytes objects: expected c-style contiguous buffer");
        return NULL;
    }

    int ret;
    int errno0;
    Py_BEGIN_ALLOW_THREADS
    if (timeout <= 0) {
        ret = mq_send(self->data.queue, (const char *) buffer.buf, buffer.len, prio);
    } else {
        struct timespec t;
        calcTimeout(timeout, &t);
        ret = mq_timedsend(self->data.queue, (const char *) buffer.buf, buffer.len, prio, &t);
    }
    errno0 = errno;
    Py_END_ALLOW_THREADS

    PyBuffer_Release(&buffer);

    if (ret != 0) {
        switch (errno0) {
            case EAGAIN:
                PyErr_SetString(PyExc_BufferError, "errno=EAGAIN: The queue was full, and the <non_blocking> flag was set");
                break;
            case ETIMEDOUT:
                PyErr_SetString(PyExc_TimeoutError, "errno=ETIMEDOUT: The call timed out before a message could be transferred");
                break;
            case EBADF:
                PyErr_SetString(PyExc_ValueError, "errno=EBADF: The descriptor specified was invalid or not opened for writing");
                break;
            case EINTR:
                PyErr_SetString(PyExc_ValueError, "errno=EINTR: The call was interrupted by a signal handler");
                break;
            case EINVAL:
                PyErr_SetString(PyExc_ValueError, "errno=EINVAL: The call would have blocked, and timeout was invalid");
                break;
            case EMSGSIZE:
                PyErr_SetString(PyExc_ValueError, "errno=EMSGSIZE: Message length was greater than the maximum message size of the message queue");
                break;
            default:
                PyErr_SetString(PyExc_ValueError, "Failed to send data to message queue");
                break;
        }
        return NULL;
    }

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject * IPCMessageQueue_recv(IPCMessageQueueData * self, PyObject * args) {
    IPC_DBG("IPCMessageQueue_recv 0\n");
    long long timeout = 0;
    Py_buffer buffer;
    if (!PyArg_ParseTuple(args, "y*L", &buffer, &timeout)) { return NULL; }
    if (!PyBuffer_IsContiguous(&buffer, 'C')) {
        PyErr_SetString(PyExc_ValueError, "Illegal bytes objects: expected c-style contiguous buffer");
        return NULL;
    }

    unsigned int prio;
    ssize_t ret;
    int errno0;
    Py_BEGIN_ALLOW_THREADS
    if (timeout <= 0) {
        ret = mq_receive(self->data.queue, (char *) buffer.buf, buffer.len, &prio);
    } else {
        struct timespec t;
        calcTimeout(timeout, &t);
        ret = mq_timedreceive(self->data.queue, (char *) buffer.buf, buffer.len, &prio, &t);
    }
    errno0 = errno;
    Py_END_ALLOW_THREADS

    PyBuffer_Release(&buffer);

    if (ret == -1) {
        switch (errno0) {
            case EAGAIN:
                PyErr_SetString(PyExc_BufferError, "errno=EAGAIN: The queue was empty, and the <non_blocking> flag was set for the message queue");
                break;
            case ETIMEDOUT:
                PyErr_SetString(PyExc_TimeoutError, "errno=ETIMEDOUT: The call timed out before a message could be transferred");
                break;
            case EBADF:
                PyErr_SetString(PyExc_ValueError, "errno=EBADF: The queue descriptor specified was invalid or not opened for reading");
                break;
            case EINVAL:
                PyErr_SetString(PyExc_ValueError, "errno=EINVAL: The call would have blocked, and timeout was invalid");
                break;
            case EMSGSIZE:
                PyErr_SetString(PyExc_ValueError, "errno=EMSGSIZE: Buffer length was less than the message size the message queue");
                break;
            default:
                PyErr_SetString(PyExc_ValueError, "Failed to read queue");
                break;
        }
        return NULL;
    }

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject * IPCMessageQueue_close(IPCMessageQueueData * self, PyObject * args) {
    if (!PyArg_ParseTuple(args, "")) { return -1; }
    IPCMessageQueue_clear(self);
    Py_INCREF(Py_None);
    return Py_None;
}

static PyMethodDef IPCMessageQueue_methods[] = {
    { "send", (PyCFunction) IPCMessageQueue_send, METH_VARARGS, "void(Buffer buffer, unsigned int priority, long long timeout) = Sends bytes to queue" },
    { "recv", (PyCFunction) IPCMessageQueue_recv, METH_VARARGS, "void(Buffer buffer, long long timeout) = Receive bytes from queue" },
    { NULL }
};

static PyTypeObject IPCMessageQueue = {
    PyVarObject_HEAD_INIT(NULL, 0)
    .tp_name = IPC_STRINGIFY(IPC_MODULE_NAME) "." "IPCMessageQueue",
    .tp_doc = "IPCMessageQueue documentation",
    .tp_basicsize = sizeof(IPCMessageQueueData),
    .tp_itemsize = 0,
    .tp_flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
    .tp_new = IPCMessageQueue_new,
    .tp_init = (initproc) IPCMessageQueue_init,
    .tp_dealloc = (destructor) IPCMessageQueue_dealloc,
    .tp_clear = (inquiry) IPCMessageQueue_clear,
    .tp_methods = IPCMessageQueue_methods,
};
