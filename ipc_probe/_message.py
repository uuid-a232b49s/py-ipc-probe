import ctypes


TYPE_LIVENESS = "livenessProbe"
TYPE_READINESS = "readinessProbe"
TYPE_STARTUP = "startupProbe"


class Message(ctypes.Structure):
    _fields_ = [
        ("type", ctypes.c_char * 32),
        ("data", ctypes.c_char * 32)
    ]


SIZE = ctypes.sizeof(Message)
