import ipc_probe_lib as _ipc
from . import _message


def receiver(queue_name, capacity):
    return _ipc.IPCMessageQueue(
        name=queue_name,
        capacity=capacity,
        msg_size=_message.SIZE,
        keep_on_del=False,
        read=True,
        write=False,
        create=True,
        exclusive=False,
        non_blocking=False
    )


def sender(queue_name):
    return _ipc.IPCMessageQueue(
        name=queue_name,
        capacity=0,
        msg_size=0,
        keep_on_del=True,
        read=False,
        write=True,
        create=False,
        exclusive=False,
        non_blocking=False
    )
