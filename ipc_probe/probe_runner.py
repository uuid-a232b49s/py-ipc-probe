import ipc_probe_lib as _ipc
from . import _message, _queue


class ProbeResponse:

    def liveness(self):
        return True

    def readiness(self):
        return True

    def startup(self):
        return True

    def custom(self, probe_type: str):
        return False


def loop(queue_name: str, loop_condition_fn=None, response: ProbeResponse = None, timeout_receive_ms=250, timeout_response_ms=250):

    if queue_name is not None and not queue_name.startswith('/'):
        queue_name = '/' + queue_name

    queue = _queue.receiver(queue_name, 10)

    msg = _message.Message()

    if response is None:
        response = ProbeResponse()

    if loop_condition_fn is None:
        loop_condition_fn = _TRUE_FN

    while loop_condition_fn():
        try:
            queue.recv(msg, timeout_receive_ms)
        except TimeoutError:
            continue

        msg_type_ = msg.type.decode('utf-8')
        if msg_type_ == "livenessProbe":
            if response.liveness():
                _try_send_response(msg, timeout_response_ms)
        elif msg_type_ == "readinessProbe":
            if response.readiness():
                _try_send_response(msg, timeout_response_ms)
        elif msg_type_ == "startupProbe":
            if response.liveness():
                _try_send_response(msg, timeout_response_ms)
        else:
            if response.custom(msg_type_):
                _try_send_response(msg, timeout_response_ms)


def _TRUE_FN():
    return True


def _try_send_response(msg, timeout):
    try:
        target_queue = _queue.sender(msg.data.decode('utf-8'))
        msg.type = bytes([1])
        target_queue.send(msg, 0, timeout)
    except (ValueError, TimeoutError):
        pass
