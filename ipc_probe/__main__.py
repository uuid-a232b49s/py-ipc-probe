import sys
import argparse


def main():
    ap = argparse.ArgumentParser(description='ipc_probe: IPC status probing')
    ap.add_argument('-q', '--queue', type=str, help='named queue to use')
    ap.add_argument('-l', '--probe-liveness', action='store_true', help='probe process liveness status')
    ap.add_argument('-r', '--probe-readiness', action='store_true', help='probe process readiness status')
    ap.add_argument('-s', '--probe-startup', action='store_true', help='probe process startup status')

    a = ap.parse_args()

    if a.probe_liveness or a.probe_readiness or a.probe_startup:
        if a.queue is None:
            sys.stderr.write('queue name is required\n')
            return 2

        from . import probe_query as pq

        ok = True

        if a.probe_liveness:
            x = _try_query(pq, a.queue, pq.TYPE_LIVENESS)
            ok = ok and x
            sys.stdout.write('liveness={}\n'.format(x))
        if a.probe_readiness:
            x = _try_query(pq, a.queue, pq.TYPE_READINESS)
            ok = ok and x
            sys.stdout.write('readiness={}\n'.format(x))
        if a.probe_startup:
            x = _try_query(pq, a.queue, pq.TYPE_STARTUP)
            ok = ok and x
            sys.stdout.write('startup={}\n'.format(x))

        return 0 if ok else 1


def _try_query(pq, queue, msg):
    try:
        pq.query(queue, pq.TYPE_LIVENESS)
    except (ValueError, TimeoutError):
        return False
    return True


if __name__ == '__main__':
    exit(main())
