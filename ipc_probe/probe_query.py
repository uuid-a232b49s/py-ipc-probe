import uuid

import ipc_probe_lib as _ipc
from . import _message, _queue

from ._message import \
    TYPE_LIVENESS, \
    TYPE_READINESS, \
    TYPE_STARTUP


def query(
    queue_name: str,
    probe_type: str,
    priority: int = 0,
    timeout_send_ms=250,
    timeout_recv_ms=250
):

    if queue_name is not None and not queue_name.startswith('/'):
        queue_name = '/' + queue_name

    target_queue = _queue.sender(queue_name)

    response_queue_name = '/' + uuid.uuid4().hex[1:]
    response_queue = _queue.receiver(response_queue_name, 1)

    msg = _message.Message()
    msg.type = probe_type.encode('utf-8')
    msg.data = response_queue_name.encode('utf-8')

    target_queue.send(msg, priority, timeout_send_ms)

    del target_queue

    response_queue.recv(msg, timeout_recv_ms)
