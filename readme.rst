py-ipc-probe
============


What is this?
-------------

- Process probing utility

  - Service process runs a queue reading loop

  - An external process can then probe the service process to retrieve its status

- Intended for kubernetes container monitoring (liveness, readiness, startup)

- IPC messaging wrapper around POSIX mq_* functions


License
-------

Boost Software License - Version 1.0


Minimal Setup
-------------

- nothing here yet ...
